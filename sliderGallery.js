;(function($, window, document, undefined){

	$.fn.sliderGallery = function(options){

		var $this = this;

		// methods
		var methods = {

			init: function(options){

				if(typeof options === 'undefined'){
					options = {};
				}

				return $this.each(function(index, gallery){

					var $gallery = $(gallery);

					// default settings
					var settings = {
						width: null,
						height: null,
						gallery: true
					};

					// check options for inherit
					if(options.width == 'inherit'){
						options.width = $this.width();
					}

					if(options.height == 'inherit'){
						if((options.gallery != null && !options.gallery) || $gallery.find('.sub-img').length == 1){
							options.height = $this.height();
						}
						else{
							options.height = $this.height() - 140;
						}
					}
					
					var $firstImg = $gallery.find('.img-list img:first').addClass('current');

					// determine width + height of first image to base component off of
					settings.height = $firstImg.height();
					settings.width = $firstImg.width();
					settings.heightOffset = $firstImg.outerHeight(true) - $firstImg.height();
					settings.widthOffset = $firstImg.outerWidth(true) - $firstImg.width();

					// settings & options
					var settings = $.extend(settings, options);

					// check for gallery option
					if(!settings.gallery){
						$gallery.find('.img-list').css('display', 'none');
						$gallery.find('.forward, .back').addClass('big-control');
					}

					// calc true width
					settings.trueHeight = settings.height + settings.heightOffset;
					settings.trueWidth = settings.width + settings.widthOffset;

					var imgListOffset = $gallery.find('.img-list').outerWidth(true) - $gallery.find('.img-list').width();
					$gallery.find('.img-list').css('width', settings.trueWidth - imgListOffset);

					// give images in list class sub-img
					// determine slide width
					var slideWidth = 0;
					$gallery.find('.img-list img').each(function(index, element){
						$(element).addClass('sub-img').attr('data-index', index);
						slideWidth += $(element).outerWidth(true);
					});

					// set first image as the main
					$('<div class="main-container"></div>').appendTo($gallery.find('.main-img')).css('width', settings.trueWidth * 3).css('left', -(settings.trueWidth)).css('height', settings.trueHeight)
						.append('<div class="img-wrapper"></div>').find('.img-wrapper').append($firstImg.clone().removeClass('sub-img')).css('left', settings.trueWidth).attr('data-index', $firstImg.attr('data-index'));

					// set height and width of image
					$gallery.find('.main-img img').css('width', settings.width).css('height', settings.height);
					$gallery.find('.main-img, .img-wrapper').css('width', settings.trueWidth).css('height', settings.trueHeight);

					methods.caption($firstImg, settings);

					if($gallery.find('.sub-img').length > 1){
						// set slide width
						$gallery.find('.img-list .slide').css('width', slideWidth);

						// position of forward button based on width and whether or not the gallery is present
						if(settings.gallery){
							$gallery.find('.forward').css('left', settings.width - ($gallery.find('.forward').outerWidth(true) * 2));
						}
						else{
							$gallery.find('.forward').css('left', settings.width - ($gallery.find('.forward').outerWidth(true) * 2) ).css('top', -((settings.height/2) + $gallery.find('.forward').outerHeight(true)/2) );
							$gallery.find('.back').css('top', -((settings.height/2) + $gallery.find('.back').outerHeight(true)/2) );
						}

						$gallery.find('.back').css('visibility', 'hidden');

						// add on click event to sub-imgs to allow switching of images
						$gallery.on('click', '.img-list .slide .sub-img:not(.current)', function() {
							if(!$gallery.find('.main-img .main-container .img-wrapper').is(':animated')){
								methods.animate($(this), $gallery, settings);
							}
							
						}).delay(1000);
						
						if(settings.gallery && ($gallery.find('.img-list .slide').width() <= $gallery.find('.img-list').width()) ){
							$gallery.find('.forward, .back').css('display', 'none');
						}

						$gallery.on('click', '.back', function(){
							if(settings.gallery){
								$gallery.find('.forward').css('visibility', 'visible');
								var right = parseInt($gallery.find('.img-list .slide').css('right')) - $gallery.find('.img-list').width();

								if(right < 0){
									$(this).css('visibility', 'hidden');
									right = 0;
								}

								$gallery.find('.img-list .slide').css('right', right);
							}
							else{
								if(!$gallery.find('.main-img .main-container .img-wrapper').is(':animated')){
									$gallery.find('.forward').css('visibility', 'visible');
									var $nextImg = $gallery.find('.img-list .current').removeClass('.current').prev().addClass('current');
									methods.animate($nextImg, $gallery, settings);

									if($nextImg.attr('data-index') == 0){
										$(this).css('visibility', 'hidden');
									}
								}
							}
						}).delay(1000);

						$gallery.on('click', '.forward', function(){
							if(settings.gallery){
								$gallery.find('.back').css('visibility', 'visible');
								var imgListWidth = $gallery.find('.img-list').width();
								var right = parseInt($gallery.find('.img-list .slide').css('right')) + imgListWidth;

								if(right + imgListWidth > $('.img-list .slide').width()){
									$(this).css('visibility', 'hidden');
									right = $gallery.find('.img-list .slide').width() - imgListWidth;
								}

								$gallery.find('.img-list .slide').css('right', right);
							}
							else{
								if(!$gallery.find('.main-img .main-container .img-wrapper').is(':animated')){
									$gallery.find('.back').css('visibility', 'visible');
									var $nextImg = $gallery.find('.img-list .current').removeClass('.current').next().addClass('current');
									methods.animate($nextImg, $gallery, settings);

									if($nextImg.attr('data-index') == $gallery.find('.img-list .sub-img').last().attr('data-index')){
										$(this).css('visibility', 'hidden');
									}
								}
							}
						}).delay(1000);
					}
					else{
						// only 1 image, remove img-list
						$gallery.find('.img-list').remove();
						$gallery.find('.forward, .back').remove();
					}
					
				});

			},
			animate: function($nextImg, $gallery, settings){
				$nextImg.parent().find('.current').removeClass('current').end().end().addClass('current');

				var $prevImg = $gallery.find('.main-container .img-wrapper').addClass('prev-img');
				
				if($nextImg.attr('data-index') > $gallery.find('.prev-img').attr('data-index')){
					// slide to left
					var slideAmountPrev = 0;
					var slideAmountNext = settings.trueWidth;
					$nextImg = $gallery.find('.main-container .img-wrapper').after('<div class="img-wrapper next-img" data-index="'+$nextImg.attr('data-index')+'"></div>').parent().find('.next-img').append($nextImg.clone().removeClass('sub-img')).css('width', settings.trueWidth).css('height', settings.trueHeight);
					$gallery.find('.next-img').css('left', settings.trueWidth * 2);
				}
				else{
					// slide to right .img-wrapper img
					$nextImg = $gallery.find('.main-container .img-wrapper').before('<div class="img-wrapper next-img" data-index="'+$nextImg.attr('data-index')+'"></div>').parent().find('.next-img').append($nextImg.clone().removeClass('sub-img')).css('width', settings.trueWidth).css('height', settings.trueHeight);
					var slideAmountPrev = parseInt($gallery.find('.prev-img').css('left')) + settings.trueWidth;
					var slideAmountNext = settings.trueWidth;
				}

				$nextImg.find('img').css('width', settings.width).css('height', settings.height);

				$prevImg.animate({
					left: [slideAmountPrev, 'swing']
					}, 1000, function(){
						$(this).remove();
					}
				);
				$nextImg.animate({
					left: [slideAmountNext, 'swing']
					}, 1000, function(){
						$(this).removeClass('next-img');
					}
				);
				
				methods.caption($nextImg.find('img'), settings);
			},
			caption: function($img, settings){
				var $caption = $('<div class="caption"></div>').appendTo('.img-wrapper[data-index="'+$img.attr('data-index')+'"]').css('width', settings.width);

				if($img.attr('data-header')){
					$('<h4>'+$img.attr('data-header')+'</h4>').appendTo($caption).css('margin-right', settings.width - $caption.find('h4').outerWidth(true));
				}

				if($img.attr('data-desc')){
					$caption.append('<p class="light">'+$img.attr('data-desc')+'</p>');
				}

				$caption.css('bottom', $caption.outerHeight(true) + 5);
			}

		}
		
		methods.init(options);

	}

}(jQuery, window, document));